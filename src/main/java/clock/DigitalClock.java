package clock;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DigitalClock extends Application {

    private static final String APPLICATION_TITLE = "Simple DigitalClock";
    private static final int WIDTH_OF_THE_WINDOW = 150;
    private static final int HEIGHT_OF_THE_WINDOW = 100;

    private Display display;

    public void start(Stage primaryStage) {
        initDisplay();
        setUpMainWindow(primaryStage);
        startTheClock();
    }

    private void initDisplay() {
        this.display = new Display();
    }

    private void setUpMainWindow(final Stage primaryStage) {
        primaryStage.setTitle(APPLICATION_TITLE);
        primaryStage.setMinWidth(WIDTH_OF_THE_WINDOW);
        primaryStage.setMinHeight(HEIGHT_OF_THE_WINDOW);
        primaryStage.setResizable(false);
        Group layout = createLayout();
        primaryStage.setScene(new Scene(layout));
        primaryStage.show();
    }

    private Group createLayout() {
        Group layout = new Group();
        layout.getChildren().add(display);
        return layout;
    }

    private void startTheClock() {
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                display.update();
            }
        }.start();
    }

}
