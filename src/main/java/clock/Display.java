package clock;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

class Display extends Label {

    private static final String DISPLAY_PATTERN = "HH:mm:ss";
    private static final String DEFAULT_DISPLAY = "00:00:00";
    private static final String FONT_NAME = "Ariel";
    private static final int FONT_SIZE = 35;
    private static final int PADDING_SIZE = 30;

    Display() {
        super(DEFAULT_DISPLAY);
        setFont(new Font(FONT_NAME, FONT_SIZE));
        setPadding(new Insets(PADDING_SIZE));
    }

    void update() {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(DISPLAY_PATTERN);
        LocalTime currentTime = LocalTime.now();
        setText(currentTime.format(timeFormatter));
    }
}
