package stopwatch;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

import java.time.Duration;

class Display extends Label {

    private static final String DISPLAY_PATTERN = "%02d:%02d:%02d";
    private static final String DEFAULT_DISPLAY = "00:00:00";
    private static final String FONT_NAME = "Ariel";
    private static final int FONT_SIZE = 35;
    private static final int PADDING_SIZE = 30;

    Display() {
        super(DEFAULT_DISPLAY);
        setFont(new Font(FONT_NAME, FONT_SIZE));
        setPadding(new Insets(PADDING_SIZE));
    }

    void update(final Duration duration) {
        String formattedTime = format(duration.getSeconds());
        setText(formattedTime);
    }

    private String format(final long seconds) {
        return String.format(DISPLAY_PATTERN, seconds / 3600, (seconds % 3600) / 60, (seconds % 60));
    }

    public void reset() {
        setText(DEFAULT_DISPLAY);
    }
}
