package stopwatch;

import javafx.scene.control.Button;

class ResetButton extends Button {

    private static final String DEFAULT_LABEL = "RESET";

    ResetButton() {
        super(DEFAULT_LABEL);
        setDisable(true);
    }
}
