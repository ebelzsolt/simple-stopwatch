package stopwatch;

import javafx.scene.control.Button;

class StartStopButton extends Button {

    private State state;

    StartStopButton() {
        this.state = State.START;
        setText(state.name());
    }

    public State getState() {
        return state;
    }

    public void switchState(final State state) {
        this.state = state;
        this.setText(state.name());
    }

}
