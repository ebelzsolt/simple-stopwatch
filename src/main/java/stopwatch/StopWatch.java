package stopwatch;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.time.Duration;
import java.time.Instant;

public class StopWatch extends Application {

    private static final String APPLICATION_TITLE = "Simple StopWatch";
    private static final int MAX_WIDTH = 330;

    private Display display;
    private StartStopButton startStopButton;
    private ResetButton resetButton;
    private AnimationTimer timer;
    private Instant startTime;

    @Override
    public void start(Stage primaryStage) {
        initComponents();
        setUpStopWatch(primaryStage);
    }

    private void initComponents() {
        this.display = new Display();
        this.startStopButton = new StartStopButton();
        this.resetButton = new ResetButton();
        this.timer = createTimer();
        startStopButton.setOnAction(new StartStopButtonEventHandler());
        resetButton.setOnAction(new ResetButtonEventHandler());
    }

    private void setUpStopWatch(final Stage primaryStage) {
        primaryStage.setTitle(APPLICATION_TITLE);
        primaryStage.setMaxWidth(MAX_WIDTH);
        primaryStage.setResizable(false);
        FlowPane layout = createLayout();
        primaryStage.setScene(new Scene(layout));
        primaryStage.show();
    }

    private FlowPane createLayout() {
        FlowPane layout = new FlowPane();
        layout.getChildren().addAll(display, startStopButton, resetButton);
        return layout;
    }

    private AnimationTimer createTimer() {
        return new AnimationTimer() {
            @Override
            public void handle(long now) {
                Instant currentTime = Instant.now();
                Duration elapsedTime = Duration.between(startTime, currentTime);
                display.update(elapsedTime);
            }
        };
    }

    private class StartStopButtonEventHandler implements EventHandler {

        @Override
        public void handle(Event event) {
            if (isStopWatchNotRunning(event)) {
                startTime = Instant.now();
                startStopButton.switchState(State.STOP);
                timer.start();
            } else {
                timer.stop();
                resetButton.setDisable(false);
            }
        }

    }

    private boolean isStopWatchNotRunning(final Event event) {
        return event.getSource() == startStopButton && startStopButton.getState() == State.START;
    }

    private class ResetButtonEventHandler implements EventHandler {

        @Override
        public void handle(Event event) {
            startTime = null;
            display.reset();
            startStopButton.switchState(State.START);
            resetButton.setDisable(true);
        }
    }
}
